﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This class is defined to consider characteristics of parcel by a user
/// </summary>
public class Parcel
{
    public double Length { get; set; }
    public double Breadth { get; set; }
    public double Height { get; set; }
    public double Weight { get; set; }
    public double Cost { get; set; }

    

}
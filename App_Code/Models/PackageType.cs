﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PackageType
/// </summary>
public static class PackageType
{
    static PackageType()
    {
        InitialPackages();
    }
    public static List<Package> Packages { get; set; }

    private static void InitialPackages()
    {
        Packages = new List<Package>();
        //Add the small package characteristics
        Packages.Add(new Package { Description = "small package", Length = 200, Breadth = 300, Height = 150, Cost = 5 });
        //Add the medium package characteristics
        Packages.Add(new Package { Description = "medium package", Length = 300, Breadth = 400, Height = 200, Cost = 7.5 });
        //Add the large package characteristics
        Packages.Add(new Package { Description = "large package", Length = 400, Breadth = 600, Height = 250, Cost = 8.5 });

    }
    public static Package findPackageType(Parcel parcel)
    {
        //find the suitable package only based on the Length of the parcel
        var lengthIndex = FindIndex(Packages.Select(p => p.Length).ToList(), parcel.Length);

        //find the suitable package only based on the Breadth of the parcel
        var BreadthIndex = FindIndex(Packages.Select(p => p.Breadth).ToList(), parcel.Breadth);

        //find the suitable package only based on the Height of the parcel
        var HeightIndex = FindIndex(Packages.Select(p => p.Height).ToList(), parcel.Height);

        //The index with the biggest value, is our package type
        int max = Math.Max(lengthIndex, Math.Max(BreadthIndex, HeightIndex));

        // if max is equal to the number of package types, it means package exceeds the defined limit.
        if (max == Packages.Count)
            return null;

        return Packages[max];

    }
    private static int FindIndex(List<double> indeexes, double value)
    {
        for (int i = 0; i < indeexes.Count; i++)
        {
            if (value <= indeexes[i])
                return i;
        }

        return indeexes.Count;
    }





}
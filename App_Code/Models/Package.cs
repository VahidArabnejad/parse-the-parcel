﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This class is defined to consider different characteristics of each package
/// </summary>
public class Package
{
    public Package() { }
    public string Description { get; set; }
    public double Length { get; set; }
    public double Breadth { get; set; }
    public double Height { get; set; }
    public double Cost { get; set; }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TradeMe.Startup))]
namespace TradeMe
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}

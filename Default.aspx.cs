﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCalc_click(object sender, EventArgs e)
    {
        /* A package can be considered as a cube, so we should sort the user inputs first.
         The smallest value is Height, secons small value is Length, and the biggest value is Breadth.
         If we dont sort the inputs, and a user enters L=200, B=500, H=300, no package can be found.
         but if we sort the data, L= 300, B=500, H=200, we can parse it as a Large package.*/

        List<double> userInputs = new List<double>();
        userInputs.Add(double.Parse(txtLength.Text));
        userInputs.Add(double.Parse(txtBreadth.Text));
        userInputs.Add(double.Parse(txtHeight.Text));
        userInputs.Sort();

        Parcel p = new Parcel
        {
            Height = userInputs[0],
            Length = userInputs[1],
            Breadth = userInputs[2],
            Weight = double.Parse(txtWeight.Text)
        };
        Package type = PackageType.findPackageType(p);

        if (type == null || p.Weight > 25)
        {
            txtResult.Text = "Unfortunatley, we can not deliver your parcel" + Environment.NewLine;
            if (p.Weight > 25)
            {
                txtResult.Text += Environment.NewLine + "Your package exceeds our weight limit";
            }
            else
            {
                txtResult.Text += Environment.NewLine + "The dimensions of your package exceed our limit";
            }
        }
        else
        {
            p.Cost = type.Cost;
            txtResult.Text = type.Description + Environment.NewLine;
            txtResult.Text += type.Cost.ToString() + "$";
        }

    }

}
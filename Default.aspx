﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>TRADEME</h1>
       </div>

    <div class="row">
        <div class="col-md-6">
            <h2>Shipment Information</h2>
            <div class="row">

                <asp:Label Text="Lengh(mm)" runat="server" />
                <asp:TextBox runat="server" ID="txtLength" />
            </div>
            <div class="row">
                <asp:Label Text="Breadth(mm)" runat="server" />
                <asp:TextBox runat="server" ID="txtBreadth" />
            </div>
            <div class="row">
                <asp:Label Text="Height(mm)" runat="server" />
                <asp:TextBox runat="server" ID="txtHeight" />
            </div>
            <div class="row">
                <asp:Label Text="Weight(KG)" runat="server" />
                <asp:TextBox runat="server" ID="txtWeight" />
            </div>

        </div>
        <div class="col-md-6">
            <div class="col-md-3">
                <asp:Button ID="btnCalc" runat="server" OnClick="btnCalc_click" Text="Calculate" />

            </div>
            <div class="col-md-3">
                <asp:Label Text="Cost Estimation" runat="server" />
                <asp:TextBox ID="txtResult" TextMode="multiline" Rows="7" runat="server" />
            </div>
        </div>

    </div>
</asp:Content>
